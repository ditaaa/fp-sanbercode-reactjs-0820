import React from 'react'
import ListGame from '../Games/ListGame'
import ListMovie from '../Movies/ListMovie'
import Header from '../../layouts/Header'


const Home = () => {
    return (
        <>
            <Header/>
            <ListMovie />
            <ListGame />
        </>
    )
}

export default Home
