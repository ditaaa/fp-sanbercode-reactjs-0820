import React from 'react';
import Main from './layouts/Main';
import {UserProvider} from "./context/UserContext"
import './App.css';
import 'antd/dist/antd.css';


function App() {
  return (
    <>
      <UserProvider>
        <Main />
      </UserProvider>
    </>

  )
}

export default App;