import React from "react"
import { Layout } from "antd"


const Footer = () => {
  const { Footer } = Layout;

  return (
    <>
    <Footer style = {{ position: 'fixed', width: '100%', bottom: '0px', height: '55px', backgroundColor: 'black', color: 'white', textAlign: 'right'}}>
      <b>Sanbercode ©2020 Created by Praditha Ayu Lestari</b>
    </Footer>
    </>
  )
}

export default Footer