import React, { useContext } from "react"
import { UserContext } from "../context/UserContext";
import { Link } from "react-router-dom";
import { Layout, Menu, Avatar, Button} from 'antd';


const Header =() => {
            const { Header } = Layout;

            const [user, setUser] = useContext(UserContext)

            const handleLogout = () => {
                setUser(null)
                localStorage.removeItem("user")
            }


return (
        <>
            <Layout>
                <Header className="header" style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
                    <div className="logo" />
                    <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1"><Link to="/">Home</Link></Menu.Item>

                        <Menu.Item key="2"><Link to="/movie">Movies</Link></Menu.Item>

                        <Menu.Item key="3"><Link to="/game">Games</Link></Menu.Item>
                        {
                            user === null && (
                                <>
                                    <Menu.Item key="4" style={{float: 'right'}}><Link to="/login">Login</Link></Menu.Item>

                                    <Menu.Item key="5" style={{float: 'right'}}><Link to="/register">Register</Link></Menu.Item>
                                </>
                            )
                        }
                        
                        {
                            user !== null && (
                                <>
                                    <Menu.Item key="6" style={{ float: 'left', position: 'sticky'}}>
                                        <Avatar size={45}>{user.name[0]} </Avatar>
                                    </Menu.Item>

                                    <Menu.Item key="7" style={{ float: 'right'}}>
                                        <Button onClick={handleLogout} >Logout</Button>
                                    </Menu.Item>

                                </>
                            )
                        }
                    </Menu>
                </Header>
            </Layout>,
        </>
    )
}


export default Header

  /*const [user, setUser] = useContext(UserContext)
  const handleLogout = () =>{
    setUser(null)
    localStorage.removeItem("user")
  }

  return(    
    <header>
      <img id="logo" src="/img/logo.png" width="200px" />
      <nav>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/about">About </Link> </li>
          { user && <li><Link to="/movies">Movie List Editor </Link></li> }
          { user === null && <li><Link to="/login">Login </Link></li> }
          { user && <li><a style={{cursor: "pointer"}} onClick={handleLogout}>Logout </a></li> }
        </ul>
      </nav>
    </header>
  )
}*/