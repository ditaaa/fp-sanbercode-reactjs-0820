import React, {useContext, useState} from "react"
import {
  Switch,
  Route,
  Redirect,
  Link,
} from "react-router-dom";


import { Layout, Menu } from 'antd';
import Home from '../pages/Home/Home'
import Login from "../pages/User/Login"
import Register from '../pages/User/Register'
import ChangePassword from '../pages/User/ChangePassword'
import Footer from './Footer'
import CreateMovie from '../pages/Movies/CreateMovie'
import Movies from '../pages/Movies/ListMovie'
import TableMovie from '../pages/Movies/TableMovie'
import SingleMovie from '../pages/Movies/SingleMovie'
import Games from '../pages/Games/ListGame'
import SingleGame from '../pages/Games/SingleGame'
import TableGame from '../pages/Games/TableGame'
import CreateGame from '../pages/Games/CreateGame'
import EditMovie from '../pages/Movies/EditMovie'
import EditGame from '../pages/Games/EditGame'
import AlertNotif from '../context/Alert'
import {UserContext} from "../context/UserContext"
import { YoutubeOutlined, RobotOutlined } from '@ant-design/icons';


const Section = () =>{

  const [user, , alert,] = useContext(UserContext);
    const { Content } = Layout;
    const { SubMenu } = Menu;
    const { Sider } = Layout;

  const PrivateRoute = ({ user, ...props }) => {
        if (user) {
            return <Route {...props} />;
        } else {
            return <Redirect to="/login" />;
        }
    };

  const LoginRoute = ({ user, ...props }) =>
        user ? <Redirect to="/" /> : <Route {...props} />;

  const [collapsed, setCollapsed] = useState(true)

  const onCollapse = collapsed => {
        console.log(collapsed);
        setCollapsed(collapsed)
    };

return (
        <>
            <Layout>
            {alert !== "" && <AlertNotif />}
                {
                    user !== null && (
                        <>
                            <Sider
                                className="site-layout-background"
                                collapsible collapsed={collapsed} onCollapse={onCollapse}>
                                <Menu
                                    mode="inline"
                                    defaultSelectedKeys={['1']}
                                    defaultOpenKeys={['sub1']}
                                    style={{ height: '100%', borderRight: 0, marginTop: '50px' }}
                                    theme="dark"
                                >
                                    <SubMenu key="sub2" icon={<YoutubeOutlined />} title="Movies">
                                        <Menu.Item key="5"><Link to="/tablemovie"> Table Movies </Link></Menu.Item>
                                        <Menu.Item key="6"><Link to="/createmovie"> Add Movie </Link></Menu.Item>
                                    </SubMenu>
                                    <SubMenu key="sub3" icon={<RobotOutlined />} title="Games">
                                        <Menu.Item key="9"><Link to="/tablegame"> Table Games </Link></Menu.Item>
                                        <Menu.Item key="10"> Add Games </Menu.Item>
                                    </SubMenu>
                                </Menu>
                            </Sider>
                        </>
                    )
                }
                <Content
                    className="site-layout-background"
                    style={{ padding: 25, marginTop: 15, minHeight: 380, overflow: 'initial'}}>
                    <section>
                        <Switch>
                            <Route exact path="/" user={user} component={Home} />
                            <Route exact path="/movie" user={user} component={Movies} />
                            <Route exact path="/game" user={user} component={Games} />
                            <Route exact path="/register" user={user} component={Register} />
                            <Route exact path="/singlemovie/:id" user={user} component={SingleMovie} />
                            <Route exact path="/singlegame/:id" user={user} component={SingleGame} />
                            <LoginRoute exact path="/login" user={user} component={Login} />
                            <PrivateRoute exact path="/changepassword" user={user} component={ChangePassword} />
                            <PrivateRoute exact path="/tablemovie" user={user} component={TableMovie} />
                            <PrivateRoute exact path="/tablegame" user={user} component={TableGame} />
                            <PrivateRoute exact path="/createmovie" user={user} component={CreateMovie} />
                            <PrivateRoute exact path="/creategame" user={user} component={CreateGame} />
                            <PrivateRoute exact path="/editmovie/:id" user={user} component={EditMovie} />
                            <PrivateRoute exact path="/editgame/:id" user={user} component={EditGame} />
                        </Switch>
                    </section>
                </Content>
            </Layout>
            <Footer />
        </>
    )
}

export default Section
