import React from "react"
import Section from './Section'
import Header from './Header'
import { BrowserRouter as Router } from "react-router-dom";


const Main = () => {
  return (
    <>
      <Router>        
        <Section />
        <Header />
      </Router>
    </>
  )
}

export default Main
